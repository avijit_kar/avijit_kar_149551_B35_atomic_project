-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2016 at 03:36 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `avijit_kar_atomic_project_b35`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `date`, `is_deleted`) VALUES
(1, 'avijit', '31.12.1993', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(4, 'kazal', 'lucky', 'No'),
(5, 'avi', 'randy1111', 'No'),
(6, 'rubel', 'php', 'No'),
(7, 'rahul', 'C++', 'No'),
(8, 'romel', 'computer graphics', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `city` varchar(150) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `is_deleted`) VALUES
(1, 'avijit', 'chittagong', '0'),
(2, 'om', 'sylhet', 'No'),
(3, 'om', 'sylhet', 'No'),
(4, 'rubel', 'rajshahi', 'No'),
(5, 'lucky', 'Dhaka', 'No'),
(6, 'shaila', 'sylhet', 'No'),
(7, 'priya', 'Dhaka', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `is_deleted`) VALUES
(1, 'avijit', 'milondas@gmail.com', 'No'),
(2, 'rubel', 'rubel@gmail.com', 'No'),
(3, 'om', 'om@gmail.com', 'No'),
(4, 'jon', 'jon@gmail.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `is_deleted`) VALUES
(1, 'avijit', 'male', 'No'),
(2, 'antika', 'Female', 'No'),
(5, 'momo', 'Female', 'No'),
(6, 'y', 'Female', '2016-11-25 22:47:56'),
(7, 'r', 'Male', 'No'),
(8, 'ui', 'Female', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `hobbies` varchar(500) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_deleted`) VALUES
(1, 'avijit', 'travelling', '2016-11-20 20:32:47'),
(2, 'momo', 'cricket,writing', 'No'),
(3, 'om', 'football,cricket', 'No'),
(4, 'romel', 'football,writing', 'No'),
(5, 'rahul', 'writing', '2016-11-20 23:10:45'),
(6, 'rahul', 'football,cricket', '2016-11-20 23:10:39'),
(8, 'rubel', 'cricket,writing', 'No'),
(9, 'baba', 'football,cricket', 'No'),
(10, 'baba', 'football,cricket', 'No'),
(11, 'shima', 'writing', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `profile_picture` varchar(200) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `author_name`, `profile_picture`, `is_deleted`) VALUES
(1, 'avijit', '', '2016-11-25 00:49:38'),
(3, 'momo', '1479672486Jellyfish.jpg', '2016-11-25 12:21:12'),
(4, 'pitu', '1480012458Desert.jpg', 'No'),
(5, 'ami', '1480015318Koala.jpg', 'No'),
(6, 'hi', '1480058414Desert.jpg', 'No'),
(11, 'kk', '1480058403Penguins.jpg', 'No'),
(12, 'uuu', '1480058318Jellyfish.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summaryoforganization`
--

CREATE TABLE `summaryoforganization` (
  `id` int(11) NOT NULL,
  `orgname` varchar(200) NOT NULL,
  `summaryorg` varchar(1000) NOT NULL,
  `is_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summaryoforganization`
--

INSERT INTO `summaryoforganization` (`id`, `orgname`, `summaryorg`, `is_deleted`) VALUES
(1, 'avijit', 'overall good work', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
