<?php

namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;

use App\Utility\Utility;
use PDO;


class Birthday extends DB
{
    public $id;
    public $name;
    public $date;

    public function __construct()
    {

        parent::__construct();

    }

    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from birthday where is_deleted = 0 ";

        $STH = $this->DBH->query($sql);


        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
public function view($fetchMode='ASSOC')
{
    $STH = $this->DBH->query('SELECT * from birthday where id='.$this->id);
    $fetchMode = strtoupper($fetchMode);
    if(substr_count($fetchMode,'OBJ') > 0)
        $STH->setFetchMode(PDO::FETCH_OBJ);
    else
        $STH->setFetchMode(PDO::FETCH_ASSOC);

    $arrOneData  = $STH->fetch();
    return $arrOneData;
}

    public function setData($postVariableData=NULL)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists('name',$postVariableData))
        {
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists('date',$postVariableData))
        {
            $this->date=$postVariableData['date'];
        }
    }
    public  function  store()
    {
        $arrData=array($this->name,$this->date);
        $sql="Insert INTO birthday(name,date) VALUE (?,?)";
        //var_dump($sql);

        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully:)");
        else
            Message::setMessage("Failed !Data has been inserted successfully:(");
        Utility::redirect('create.php');
    }//end of store method


    public function update(){
        $arrData=array($this->name,$this->date);

        //UPDATE `atomic_project_35`.`book_title` SET `book_title` = 'nan' WHERE `book_title`.`id` = 4;

        $sql="update birthday SET  name=? ,date=? WHERE id=".$this->id;
        $STH =$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }//end of update();

    public function delete(){
        $sql="Delete from birthday where id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }//end of delete();

    public function trash(){

        $sql = "Update birthday SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()


}// end of BookTitle class