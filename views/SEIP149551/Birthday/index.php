<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<?php
require_once("../../../vendor/autoload.php");


use App\Birthday\Birthday;
use App\Message\Message;

$objBookTitle = new Birthday() ;
$allData=$objBookTitle->index("obj");
$serial=1;


################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $objBookTitle->search($_REQUEST);
$availableKeywords=$objBookTitle->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################



echo "<table border='2px'>";
echo"<th>serial</th><th>id</th><th>name</th><th>date</th><th colspan='2'>Action</th>";
foreach($allData as $oneData)
{
    echo "<tr>";
    echo"<td>$serial</td>";
    echo"<td>$oneData->id</td>";
    echo"<td>$oneData->name</td>";
    echo"<td>$oneData->date</td>";
    echo "
<td>
    <a href='view.php?id=$oneData->id'><button class='btn-info'>view</button></a>
    <a href='edit.php?id=$oneData->id'><button class='btn-primary'>edit</button></a>
     <a href='trash.php?id=$oneData->id'><button class='btn-success'>trash</button></a>
    <a href='delete.php?id=$oneData->id'><button class='btn-danger'>delete</button></a>
    </a>
    ";

    echo"</tr>";
    $serial++;

   //$oneDate->book_title."".$oneData->author_name."<br>";
}
echo "</table>"
    ?>

<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="left" class="container">
    <ul class="pagination">

        <?php
         echo '<li><a href="">' . "Previous" . '</a></li>';
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        echo '<li><a href="">' . "Next" . '</a></li>';
        ?>

<select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
    <?php
    if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
    else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

    if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
    else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

    if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
    else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

    if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
    else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

    if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
    else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

    if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
    else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
    ?>
</select>
</ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->



</body>
</html>




<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->
